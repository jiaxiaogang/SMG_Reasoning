//
//  Understand+Input.h
//  SMG_NothingIsAll
//
//  Created by 贾  on 2017/5/6.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Understand.h"

/**
 *  MARK:--------------------输入理解--------------------
 *
 *
 *
 */
@interface Understand (INPUT)


/**
 *  MARK:--------------------Feel->Understand->Store--------------------
 */
-(void) commitWithFeelModelArr:(NSArray*)modelArr;


@end



